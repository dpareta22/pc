class User < ApplicationRecord
  has_many :posts
  has_many :comments
  has_one_attached :avatar
  acts_as_voter
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :first_name, :last_name, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }
  validates :age, numericality: { only_integer: true }
  validates :gender, inclusion: { in: %w(M F m f Male Female male female), message: "Not a Valid Gender" }
  validates :user_name, format: { with: /\A^[A-Z][A-Za-z]*\z/ , message: "First CAPITAL letter" }

end
