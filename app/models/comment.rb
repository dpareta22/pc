class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user
  has_many :notification
  acts_as_votable

  validates_presence_of :description, message: 'can not be blank'
  validate :post_id_exists, on: :create

  after_create :comment_counter_inc
  after_destroy :comment_counter_dec

  def comment_counter_inc
    obj = self.post
    obj.no_of_comments = obj.no_of_comments + 1
    obj.save
  end

  def comment_counter_dec
    obj = self.post
    obj.no_of_comments = obj.no_of_comments - 1
    obj.save
  end
  
  def post_id_exists
    return false if Post.find_by_id(self.post_id).nil?
  end

end

