class Post < ApplicationRecord
  belongs_to :user
  has_many :comments
  has_one_attached :image
  acts_as_votable

  validates :name, presence: true
  validates :description, presence: true
  validate :user_id_exists, on: :create

  scope :publicpost, -> { where(post_type: 'public') }

  def user_id_exists
    return false if User.find_by_id(self.user_id).nil?
  end

  def public?
    self.post_type == 'public'
  end
end
