class Notification < ApplicationRecord
  has_many :comments
  after_create_commit { NotificationBroadcastJob.perform_later self}
end
