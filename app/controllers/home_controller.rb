class HomeController < ApplicationController
  before_action :find_post, only: [:index]

  def index
    @pagy, @posts = pagy(Post.publicpost, items: 5)
  end

  private

  def find_post
    @post = Post.publicpost.where(id: params[:post_id])
  end
end
