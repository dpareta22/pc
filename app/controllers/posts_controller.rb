class PostsController < ApplicationController
  before_action :find_post, only: [:edit, :show, :update, :destroy, :upvote, :downvote]
  before_action :find_comment, only: [:show]

  def index
    @pagy, @posts = pagy(current_user.posts, items: 5)
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new(post_params)
    if @post.save
      flash[:notice] = "Post created"
      redirect_to @post
    else
      flash[:notice] = "Post Not created"
      render :new
    end
  end

  def show
    flash[:notice] = "Post created"
    @comments = @post.comments
    @comment = Comment.new
  end

  def update
    if @post.update(post_params)
      flash[:notice] = "Post Updated"
      redirect_to @post
    else
      flash[:danger] = "Post Not Updated"
      redirect_to @post
    end
  end

  def destroy
    if @post.destroy
      flash[:notice] = "Post destroyed"
      redirect_to posts_path
    else
      flash[:danger] = "Post Not Destroyed"
      redirect_to posts_path
    end
  end

  def upvote 
    @post.upvote_by current_user
    redirect_to post_path
  end  

  def downvote
    @post.downvote_by current_user
    redirect_to post_path
  end

  private

  def post_params
    params.require(:post).permit(:name, :description, :image, :post_type)
  end

  def find_post
    @post = Post.find_by_id(params[:id])
  end

  def find_comment
    @comment = Comment.find_by_id(params[:id])
  end
end

