class CommentsController < ApplicationController
  before_action :find_comment, only: [:edit, :show, :update, :destroy, :upvote, :downvote]
  before_action :find_post, only: [:new, :create, :edit, :update, :upvote, :downvote]
  after_action :user_notification_generate, only: [:create]

  def new
    @comment = Comment.new
  end

  def create
    if current_user
      @comment = current_user.comments.new(comment_params)
      @comment.save
    else
      flash[:notice] = "You must login to Comment..!! "
      redirect_to new_user_session_path
    end
  end

  def edit; end

  def show;  end

  def update
    flash[:notice] =
      if @comment.update(comment_params)
        "Comment Updated"
      else
        "Comment Not Updated"
      end
    redirect_to post_path(@comment.post_id)
  end

  def destroy
    if @comment.destroy
      flash[:notice] = "Comment Destroyed"
      redirect_to post_path(@comment.post_id)
    else
      flash[:notice] = "Comment Not Destroyed"
      redirect_to post_path(@comment.post_id)
    end
  end

  def upvote
    @comment.upvote_by current_user
    redirect_to post_path(@post)
  end  

  def downvote
    @comment.downvote_by current_user
    redirect_to post_path(@post)
  end

  private

  def find_post
    @post = Post.find_by_id(params[:post_id])
  end

  def find_comment
    @comment = Comment.find_by_id(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:post_id, :description)
  end
end
