
require 'rails_helper'

RSpec.describe User, type: :model do


  before(:all) do
    @user1 = create(:user)
  end

  it "has a valid factory" do
    expect(@user1).to be_valid
  end

  it "has a null first_name" do
    user2 = build(:user, first_name: "")
    expect(user2).to_not be_valid
  end

  it "has a null last_name" do
    user2 = build(:user, last_name: "")
    expect(user2).to_not be_valid
  end

  it "has a null email" do
    user2 = build(:user, email: "")
    expect(user2).to_not be_valid
  end

  it "has a null user_name" do
    user2 = build(:user, user_name: "")
    expect(user2).to_not be_valid
  end

  it "has a null gender" do
    user2 = build(:user, gender: "")
    expect(user2).to_not be_valid
  end

  it "has a null age" do
    user2 = build(:user, age: " ")
    expect(user2).to_not be_valid
  end

  it "has a null password" do
    user2 = build(:user, password: "")
    expect(user2).to_not be_valid
  end

  # #first name checks
  # it "has invalid first_name" do
  #   user2 = build(:user, first_name: "")
  #   expect(user2).to_not be_valid
  # end

end