FactoryBot.define do
  
  factory :user do
    user_name { "Test" }
    first_name { "testing" }
    last_name { "testing" }
    age { 22 }
    gender { "male" }
    email { "test@gmail.com" }
    password { "123456" }
  end
end