class CreateUserNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :user_notifications do |t|
      t.references :comment_id, foreign_key: true
      t.text :message

      t.timestamps
    end
  end
end
